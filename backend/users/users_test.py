from flask import url_for

from app.common import TRTestCase
from models.user import User


class UsersTest(TRTestCase):
    """ Testing users endpoint """

    def after_setUp(self):
        with self.app.app_context():
            self.list_url = url_for('users.get_users')
            self.detail_url = url_for('users.get_user', userid=1)
            self.delete_url = url_for('users.delete_user', userid=1)
            self.add_url = url_for('users.add_user')
            self.edit_url = url_for('users.edit_user', userid=2)

            self.restricted_get = (
                self.list_url,
                self.detail_url)

    def test_users_list(self):
        """ should return users list """
        self.create_admin()
        self.login()
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json) > 0)

    def test_user_not_found(self):
        """ should return 404 when not found """
        self.create_admin()
        self.login()

        url = url_for('users.get_user', userid=self.user.id + 1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_user_details(self):
        """ should return user details """
        self.create_admin()
        self.login()
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['username'], self.user.username)
        self.assertEqual(response.json['email'], self.user.email)
        self.assertEqual(response.json['id'], self.user.id)

    def test_delete_user_access_restriction(self):
        """ should be protected """
        response = self.client.delete(self.delete_url)
        self.assertEqual(response.status_code, 401)

    def test_delete_user_not_found(self):
        """ should return 404 when trying to delete no existing user """
        self.create_admin()
        self.login()
        url = url_for('users.delete_user', userid=2)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 404)

    def test_delete_user(self):
        """ should delete user from database """
        self.create_admin()
        self.login()
        self.create_test_user()
        url = url_for('users.delete_user', userid=self.test_user.id)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200)
        user = User.query.get(self.test_user.id)
        self.assertIsNone(user)

    def test_add_user_access_restriction(self):
        """ should be protected """
        response = self.client.post(self.add_url, data={})
        self.assertEqual(response.status_code, 401)

    def check_user_validation(self, url):
        """ helper method for user post validation tests """
        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 400)

        required_data = {
            'username': 'username',
            'first_name': 'first_name',
            'last_name': 'last_name',
            'email': 'some@email',
        }

        for key in required_data.keys():
            invalid_data = required_data.copy()
            del invalid_data[key]
            response = self.client.post(url, data=invalid_data)
            self.assertEqual(response.status_code, 400)

    def test_add_user_validation(self):
        """ should return error on wrong data """
        self.create_admin()
        self.login()
        self.check_user_validation(self.add_url)

    def test_add_user(self):
        """ should add user to database """
        user_data = {
            'username': 'test',
            'first_name': 'test first_name',
            'last_name': 'test_last_name',
            'email': 'test@email'
        }

        self.create_admin()
        self.login()

        response = self.client.post(self.add_url, data=user_data)
        self.assertEqual(response.status_code, 200)

        for key in user_data.keys():
            self.assertEqual(response.json[key], user_data[key])
        self.assertIsNotNone(response.json.get('id'))

    def test_add_user_with_defualt_password(self):
        """ should set default user password """
        user_data = {
            'username': 'test',
            'first_name': 'test first_name',
            'last_name': 'test_last_name',
            'email': 'test@email'
        }

        self.create_admin()
        self.login()

        response = self.client.post(self.add_url, data=user_data)
        user = User.query.get(response.json['id'])
        self.assertIsNone(user.password)

        user_data['password'] = 'password'
        user_data['username'] = 'user2'
        user_data['email'] = 'user2@email'
        response = self.client.post(self.add_url, data=user_data)
        user = User.query.get(response.json['id'])
        self.assertIsNotNone(user.password)

    def test_update_user_access_restriction(self):
        """ should be protected """
        response = self.client.post(self.edit_url, data={})
        self.assertEqual(response.status_code, 401)

    def test_update_user_validation(self):
        """ should return error on wrong data """
        self.create_admin()
        self.login()
        self.create_test_user()
        self.check_user_validation(self.edit_url)

    def test_update_user_not_found(self):
        """ should return 404 when user not foung """
        self.create_admin()
        self.login()
        user_data = {
            'username': 'test',
            'first_name': 'test first_name',
            'last_name': 'test_last_name',
            'email': 'test@email'
        }
        url = url_for('users.edit_user', userid=123)
        response = self.client.post(url, data=user_data)
        self.assertEqual(response.status_code, 404)

    def test_update_user(self):
        """ should update user data """
        self.create_admin()
        self.login()
        self.create_test_user()

        user_data = {
            'username': 'test',
            'first_name': 'test first_name',
            'last_name': 'test_last_name',
            'email': 'test@email'
        }
        response = self.client.post(self.edit_url, data=user_data)
        self.assertEqual(response.status_code, 200)
        user = User.query.get(2)
        self.assertEqual(response.json['id'], user.id)
        for key in user_data.keys():
            self.assertEqual(getattr(user, key), user_data[key])
            self.assertEqual(response.json[key], user_data[key])

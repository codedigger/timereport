"""
Handling all /users calls
"""
from flask import jsonify, request
from flask.ext.login import login_required
from cerberus import Validator

from app import db
from app.common import get_json_data
from models.user import User
from . import bp


base_schema = {
    'username': {
        'type': 'string',
        'empty': False,
        'required': True, },
    'first_name': {
        'type': 'string',
        'empty': False,
        'required': True, },
    'last_name': {
        'type': 'string',
        'empty': False,
        'required': True, },
    'email': {
        'type': 'string',
        'empty': False,
        'required': True, },
}


@bp.route('', methods=['GET'])
@login_required
def get_users():
    """ List all users """
    return jsonify(users=[user.to_json() for user in User.query.all()])


@bp.route('', methods=['POST'])
@login_required
def add_user():
    """ Create new user """
    data = get_json_data(request)
    if not data:
        return jsonify(message='No POST data'), 400

    schema = base_schema.copy()
    schema['password'] = {
        'type': 'string',
        'empty': False,
    }
    validator = Validator(schema)
    if not validator.validate(data):
        return jsonify(validator.errors), 400

    user = User()
    user.first_name = validator.current['first_name']
    user.last_name = validator.current['last_name']
    user.username = validator.current['username']
    user.email = validator.current['email']

    password = validator.current.get('password')
    if password is not None:
        user.set_password(password)

    db.session.add(user)
    db.session.commit()

    return jsonify(user.to_json())


@bp.route('/<int:userid>', methods=['GET'])
@login_required
def get_user(userid):
    """ Get user details """
    user = User.query.get(userid)
    if not user:
        return jsonify(message='User not found'), 404

    return jsonify(user.to_json())


@bp.route('/<int:userid>', methods=['POST'])
@login_required
def edit_user(userid):
    """ Edit user """
    data = get_json_data(request)
    if not data:
        return jsonify(message='No POST data'), 400

    schema = base_schema.copy()
    validator = Validator(schema)
    if not validator.validate(data):
        return jsonify(validator.errors), 400

    user = User.query.get(userid)
    if not user:
        return jsonify(message='User not found'), 404

    user.first_name = validator.current['first_name']
    user.last_name = validator.current['last_name']
    user.username = validator.current['username']
    user.email = validator.current['email']

    db.session.add(user)
    db.session.commit()

    return jsonify(user.to_json())


@bp.route('/<int:userid>', methods=['DELETE'])
@login_required
def delete_user(userid):
    """ Delete user """
    user = User.query.get(userid)
    if not user:
        return jsonify(message='User not found'), 404

    db.session.delete(user)
    db.session.commit()

    return jsonify(message='User deleted')

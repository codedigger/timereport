"""
Handling all user-based api calls
"""
from flask import Blueprint

bp = Blueprint('users', __name__)

# adding api points
from . import users  # noqa

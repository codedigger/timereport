from flask import url_for

from app.common import TRTestCase


class TestAuthentication(TRTestCase):
    """ Testing user authentication """

    def test_invalid_login_or_password(self):
        """ should return 401 on invalid login/password """
        self.create_admin()
        url = url_for('login.login')

        response = self.client.post(url, data={'username': 'admin', 'password': 'invalid'})
        self.assertEqual(response.status_code, 401)

        response = self.client.post(url, data={'username': 'invalid', 'password': 'admin'})
        self.assertEqual(response.status_code, 401)

        response = self.client.post(url, data={'username': 'invalid', 'password': 'invalid'})
        self.assertEqual(response.status_code, 401)

    def test_login_validation(self):
        """ should return 400 on validation error """
        self.create_admin()
        url = url_for('login.login')

        response = self.client.post(url, data={'username': 'admin'})
        self.assertEqual(response.status_code, 400)

        response = self.client.post(url, data={'password': 'admin'})
        self.assertEqual(response.status_code, 400)

    def test_current_user_restricted_when_not_logged_in(self):
        """ should return 401 when not logged in """
        url = url_for('login.get_current_user')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)

    def test_current_user(self):
        """ should return currently logged in used """
        self.create_admin()
        self.login()
        url = url_for('login.get_current_user')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.json.get('id'))

    def test_logout(self):
        """ should remove session """
        self.create_admin()
        url = url_for('login.logout')
        current_url = url_for('login.get_current_user')
        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 200)

        response = self.client.get(current_url)
        self.assertEqual(response.status_code, 401)

        self.login()
        response = self.client.get(current_url)
        self.assertEqual(response.status_code, 200)

        self.client.post(url, data={})
        response = self.client.get(current_url)
        self.assertEqual(response.status_code, 401)

    def test_session(self):
        """ should correctly return logged in user """
        self.create_admin()
        self.create_test_user()
        self.login()

        login_url = url_for('login.login')
        logout_url = url_for('login.logout')
        current_url = url_for('login.get_current_user')

        response = self.client.get(current_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['id'], 1)

        self.client.post(logout_url, data={})
        self.client.post(login_url, data={'username': 'test', 'password': 'test'})
        response = self.client.get(current_url)
        self.assertEqual(response.json['id'], 2)

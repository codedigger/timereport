"""
Handling authentication requests (login and logout)
"""
import bcrypt
from flask import jsonify, request
from flask.ext.login import login_user, logout_user, current_user
from cerberus import Validator

from app.common import get_json_data

from models.user import User
from . import bp


@bp.route('/login', methods=['POST'])
def login():
    """ Handling login to system """
    schema = {
        'username': {
            'type': 'string',
            'required': True,
            'empty': False},
        'password': {
            'type': 'string',
            'required': True,
            'empty': False},
    }

    # validating data
    validator = Validator(schema)
    if not validator.validate(get_json_data(request)):
        return jsonify(validator.errors), 400

    # simple validating
    username = validator.current['username']
    password = validator.current['password']

    # let's use one message
    error_message = 'Invalid username or password'

    user = User.query.filter_by(username=username).first()
    if user is None:
        return jsonify(message=error_message), 401

    if not bcrypt.checkpw(password, user.password):
        return jsonify(message=error_message), 401

    login_user(user)

    return jsonify(user.to_json())


@bp.route('/logout', methods=['POST'])
def logout():
    """ Handling logout from system """
    logout_user()
    return jsonify(message='user logged out')


@bp.route('/current_user', methods=['GET'])
def get_current_user():
    """ Returning currently logged in user """
    if not current_user.is_authenticated:
        return jsonify(message='Anonymouse user have no data'), 401
    return jsonify(id=current_user.get_id())

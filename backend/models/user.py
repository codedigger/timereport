"""
Implemenitng user- and login- related models
"""
import bcrypt
from flask.ext.login import UserMixin

from app import db


class User(db.Model, UserMixin):
    """ Default user class """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    password = db.Column(db.String(25), unique=False)
    email = db.Column(db.String(25), unique=True)

    def get_id(self):
        return self.id

    def set_password(self, password):
        """ Setting new password for user """
        self.password = bcrypt.hashpw(password, bcrypt.gensalt())

    def to_json(self):
        """ Returning user data in json format """
        return {
            'id': self.id,
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email}

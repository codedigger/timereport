"""
Commont functions and classes for whole project
"""
from flask.ext.testing import TestCase

from models.user import User
from . import create_app, db


def get_json_data(request):
    """
    Gets json data from request.
    We can have data in two places: request.get_json() or request.form.
    When no data was found return emty dictionary
    """
    data = request.get_json() or request.form.to_dict()
    return data or {}


class TRTestCase(TestCase):
    """ Common test class """

    restricted_get = []
    user = None
    test_user = None

    def setUp(self):
        db.create_all()
        self.after_setUp()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def after_setUp(self):
        pass

    def create_app(self):
        return create_app('test.cfg')

    def create_admin(self):
        user = User()
        user.username = 'admin'
        user.email = 'admin@admin.com'
        user.set_password('admin')

        db.session.add(user)
        db.session.commit()
        self.user = user

    def create_test_user(self):
        user = User()
        user.username = 'test'
        user.email = 'test@test.com'
        user.set_password('test')

        db.session.add(user)
        db.session.commit()
        self.test_user = user

    def login(self):
        self.client.post('/api/v1/login', data={
            'username': 'admin',
            'password': 'admin'})

    def test_access_restrictions(self):
        """ testing access restrictions """
        for url in self.restricted_get:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 401)

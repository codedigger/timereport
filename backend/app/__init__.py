"""
Basic backend function
"""
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app(config_filename):
    """ Creating Flask application with configuration from .cfg file """
    from flask import Flask

    app = Flask(__name__)
    app.config.from_pyfile(config_filename)

    # initializing database
    db.init_app(app)

    # initializng login manager
    from flask.ext.login import LoginManager
    from models.user import User
    login_manager = LoginManager()
    login_manager.init_app(app)

    @login_manager.user_loader
    def check_user(user_id):
        return User.query.get(user_id)

    # registering blueprints
    from login import bp as login_bp
    from users import bp as users_bp
    app.register_blueprint(login_bp, url_prefix='/api/v1')
    app.register_blueprint(users_bp, url_prefix='/api/v1/users')

    return app

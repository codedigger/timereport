#! /usr/bin/env python3
"""
Managing backend
"""
import argparse
import sys


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Managing timeReport backend')
    parser.add_argument('--initdb', action='store_true', help='initializing database')
    parser.add_argument('--dropdb', action='store_true', help='purging database')
    parser.add_argument('--test', action='store_true', help='start unit tests and exit')
    parser.add_argument('--html_report', action='store_true', help='generate html test coverage report after tests')

    args = parser.parse_args()

    if args.test:
        # starting configured unit tests
        import coverage
        import unittest
        # starting test coverage
        cov = coverage.Coverage(include='./*')
        cov.start()

        # starting unittests
        loader = unittest.TestLoader()
        suite = loader.discover('.', pattern='*_test.py')
        result = unittest.TextTestRunner(verbosity=1).run(suite)
        if not result.wasSuccessful():
            sys.exit(1)

        # print coverage report
        cov.stop()
        cov.report()

        if args.html_report:
            cov.html_report()

        # testin PEP8
        from flake8.main import check_file as flake8_check
        import os
        flake8_errors = 0
        for root, dirs, files in os.walk('.'):
            for file_ in files:
                if file_.endswith('.py'):
                    # we are ignoring E501
                    flake8_errors += flake8_check(os.path.join(root, file_), ignore=('E501', ))
        if flake8_check:
            sys.exit(2)

        sys.exit(0)

    from app import create_app, db
    application = create_app('default.cfg')

    if args.dropdb:
        with application.app_context():
            db.drop_all()

    if args.initdb:
        from app import create_app, db
        application = create_app('default.cfg')
        with application.app_context():
            # creating new db
            print('Creating new database in {0}'.format(application.config['SQLALCHEMY_DATABASE_URI']))
            db.create_all()

            # creating default admin user
            print('Creating default admin user')
            from models.user import User
            admin = User()
            admin.username = 'admin'
            admin.email = 'admin@admin.com'
            admin.first_name = 'Admin'
            admin.last_name = 'Admin'
            admin.set_password('admin')
            db.session.add(admin)
            db.session.commit()

    # starting main app
    application.run()

axios = require 'axios'


class User
    constructor: (@id, @username, @email) ->

    # fill user with data from response
    fromResponse: (response) ->
        @id = response.id
        @username = response.username
        @email = response.email
        @firstName = response.first_name
        @lastName = response.last_name

    # fill user data with data from API
    refresh: ->
        ret = axios.get '/api/v1/users/' + @id
        ret
        .then (response) =>
            @fromResponse response.data
        .catch (err) ->
            console.log err
        ret

    # save user in database
    save: (password=null) ->
        userData =
            first_name: @firstName
            last_name: @lastName
            username: @username
            email: @email
        userData.password = password if password

        ret = if @id then axios.post '/api/v1/users/' + @id, userData else axios.post '/api/v1/users', userData
        ret
        .then (response) =>
            @fromResponse response.data
        .catch (err) ->
            console.log err
        ret

    # delete user from database
    delete: () ->
        ret = axios.delete '/api/v1/users/' + @id
        ret
        .then (response) =>
            @id = null
        ret


module.exports = User

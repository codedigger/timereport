axios = require 'axios'
User = require './user.coffee'


class UserLibrary extends Array
    constructor: () ->

    # get all users from database
    refreshUsers: () ->
        ret = axios.get '/api/v1/users'
        ret
        .then (response) =>
            for user in response.data.users
                newUser = new User()
                newUser.fromResponse user
                @push newUser
        .catch (err) ->
            console.log err
        ret

    deleteUser: (user) ->
        if @splice @indexOf user, 1
            return true
        false


module.exports = UserLibrary


React = require 'react'
ReactDOM = require 'react-dom'
ReactRouter = require 'react-router'

LoginPage = require './pages/login.cjsx'
WorkspacePage = require './pages/workspace.cjsx'
UserManagementPage = require './pages/userManagement.cjsx'
NotFoundPage = require './pages/404.cjsx'
HeaderComponent = require './views/header.cjsx'
Session = require './services/session.coffee'

{ Router, Route, IndexRoute }  = ReactRouter
{browserHistory} = ReactRouter
{div} = React.DOM
{withRouter} = ReactRouter


class MainApp extends React.Component
    constructor: (props) ->
        super props

    render: =>
        <div key='pageContainer'>
            { @props.header if @props.header }
            { @props.mainContent if @props.mainContent }
        </div>


# checking if session is authenticated
checkAuth = (nextState, replace, callback) ->
    session = Session.get()
    session.currentUser()
    .then (data) ->
        callback()
    .catch (err) ->
        replace
            pathname: '/login'
            state: { nextPathname: nextState.location.pathname }
        callback()

routes = (
    <Router history={browserHistory}>
        <Route path='/' component={MainApp}>
            <IndexRoute components={{mainContent: WorkspacePage, header: HeaderComponent}} onEnter={checkAuth} />
            <Route path={LoginPage.url} components={{mainContent: LoginPage, header: null}} />
            <Route path={UserManagementPage.url} components={{mainContent: UserManagementPage, header: HeaderComponent}} />
            <Route path='*' components={{mainContent: NotFoundPage, header: null}} />
        </Route>
    </Router>
)
ReactDOM.render routes, document.getElementById 'app'

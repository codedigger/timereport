axios = require 'axios'

User = require './../models/user.coffee'


class Session
    instance = null

    class Session_

        constructor: () ->
            @user = null  # currently logged in user

        # login to system and setting up current user
        login: (username, password) ->
            # login to system
            response = axios.post '/api/v1/login',
                username: username
                password: password
            .then (response) =>
                # on success save current user is session
                @user = new User()
                @user.fromResponse response.data

            response

        # logout from system
        logout: ->
            response = axios.post '/api/v1/logout'
            response.then =>
                @user = null
            response

        # get user from saved session
        currentUser: ->
            return Promise.resolve @user if @user
            new Promise (resolve, reject) =>
                response = axios.get '/api/v1/current_user'
                .then (response) =>
                    @user = new User()
                    @user.fromResponse response.data
                    resolve @user
                .catch (err) ->
                    reject err

    @get: () ->
        instance ?= new Session_()

module.exports = Session

React = require 'react'
ReactRouter = require 'react-router'
ReactBootstrap = require 'react-bootstrap'

Session = require './../services/session.coffee'

{Col, Button, Panel, Form, FormGroup, FormControl, Well} = ReactBootstrap
{p} = React.DOM
{ withRouter } = ReactRouter

class LoginComponent extends React.Component
    @url: '/login'

    constructor: (props) ->
        super props
        @state =
            loginState: null
            loginErrorMessage: ''

        @username = 'username'
        @password = 'password'

    # handling username changes
    changedUsername: (event) =>
        @username = event.target.value

    # handling password changes
    changedPassword: (event) =>
        @password = event.target.value

    # checking credintetials
    login: (event) =>
        # getting session and making request
        session = Session.get()
        session.login @username, @password
        .then (data) =>
            # handling response
            @props.router.push('/')
        .catch (err) =>
            # recognize invalid username or password
            console.log err
            if err.status == 401
                @setState
                    loginState: 'error'
                    loginErrorMessage: 'Invalid username or password'
            # else we have server error
            else
                @setState
                    loginState: 'error'
                    loginErrorMessage: 'Error while connecting to server'

    ###
    # Render part
    ###
    # creating username field
    usernameField: ->
        <FormGroup key='usernameSection' controlID='usernameSection' validationState=@state.loginState>
            <Col key='usernameLabel' sm=3 componentClass=ReactBootstrap.ControlLabel>Username</Col>
            <Col key='unsernameField' sm=9>
                <FormControl type='text' placeholder='username' onChange=@changedUsername />
            </Col>
        </FormGroup>

    # creating password field
    passwordField: ->
        <FormGroup key='passwordSection' controlID='passwordSection' validationState=@state.loginState>
            <Col key='passwordLabel' sm=3 componentClass=ReactBootstrap.ControlLabel>Password</Col>
            <Col key='passwordField' sm=9>
                <FormControl type='password' placeholder='password' onChange=@changedPassword />
            </Col>
        </FormGroup>

    # creating login form
    loginForm: ->
        <Form key='loginForm' horizontal>
            { @usernameField() }
            { @passwordField() }
        </Form>

    # main render function
    render: ->
        <Panel key='loginPanel' className='loginPanel'>
            { @loginForm() }

            { <p key='loginErrorMessage' className='pull-left'>{@state.loginErrorMessage}</p> if @state.loginState }

            <Button key='loginButton' bsStyle='primary' className='pull-right' onClick=@login>Login</Button>
        </Panel>

module.exports = withRouter LoginComponent

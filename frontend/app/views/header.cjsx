React = require 'react'
ReactRouter = require 'react-router'
ReactBootstrap = require 'react-bootstrap'

Session = require './../services/session.coffee'
UserManagementPage = require './../pages/userManagement.cjsx'

{withRouter} = ReactRouter
{Navbar, Nav, NavItem, NavDropdown, MenuItem } = ReactBootstrap
{a} = React.DOM


class HeaderComponent extends React.Component
    constructor: (props) ->
        super props

    logout: =>
        session = Session.get()
        session.logout()
        .then () =>
            @props.router.push('/login')
        .catch (err) ->
            console.log err

    goUserManagement: =>
        @props.router.push(UserManagementPage.url)

    ###
    # Render part
    ###
    # right side
    rightSide: ->
        <Nav pullRight>
            <NavItem eventKey=1 onClick={@goUserManagement}>User management</NavItem>
            <NavItem eventKey=2 onClick={@logout}>Logout</NavItem>
        </Nav>

    # dropdown
    dropDown: ->
        <NavDropdown eventKey=3 title='Dropdown' id='basic-nav-dropdown'>
            <MenuItem eventKey=3.1>Action</MenuItem>
            <MenuItem eventKey=3.2>Another action</MenuItem>
            <MenuItem eventKey=3.3>Something else here</MenuItem>
            <MenuItem divider />
            <MenuItem eventKey=3.3>Separated link</MenuItem>
        </NavDropdown>

    # left side
    leftSide: ->
        <Nav>
            <NavItem eventKey=1 href='#'>Link</NavItem>
            <NavItem eventKey=2 hre='#'>Link</NavItem>
            {@dropDown()}
        </Nav>

    # main render function
    render: ->
        <Navbar inverse>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href='#'>TR2 | Time reporting</a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                { @leftSide() }
                { @rightSide() }
            </Navbar.Collapse>
        </Navbar>

module.exports = withRouter HeaderComponent

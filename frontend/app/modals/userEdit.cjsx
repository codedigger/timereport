React = require 'react'
ReactBootstrap = require 'react-bootstrap'

UserLibrary = require './../models/userLibrary.coffee'
User = require './../models/user.coffee'

{Modal, Button} = ReactBootstrap
{Form, FormGroup, Col, FormControl} = ReactBootstrap


class UserEditModal extends React.Component
    constructor: (props) ->
        super props
        @state =
            show: false

        @username = ''
        @firstName = ''
        @lastName = ''
        @email = ''

    # Close modal window
    close: =>
        @props.close()

    # updating user
    save: =>
        user = @props.user
        user.username = @username || @props.user.username
        user.firstName = @firstName || @props.user.firstName
        user.lastName = @lastName || @props.user.lastName
        user.email = @email || @props.user.email
        user.save()
        .then (response) =>
            @props.saved user

    # field change binginds
    onUsernameChange:   (event) => @username = event.target.value
    onEmailChange:      (event) => @email = event.target.value
    onFirstNameChange:  (event) => @firstName = event.target.value
    onLastNameChange:   (event) => @lastName = event.target.value
    onPasswordChange:   (event) => @password = event.target.value

    # creating username field
    renderUsername: ->
        <FormGroup controlID='usernameSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Username</Col>
            <Col sm=9>
                <FormControl
                        type='text'
                        placeholder='username'
                        onChange={@onUsernameChange}
                        defaultValue={@props.user.username} />
            </Col>
        </FormGroup>

    # creating email field
    renderEmail: ->
        <FormGroup controlID='emialSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Email</Col>
            <Col sm=9>
                <FormControl
                        type='text'
                        placeholder='email'
                        onChange={@onEmailChange}
                        defaultValue={@props.user.email} />
            </Col>
        </FormGroup>

    # creating first name field
    renderFirstName: ->
        <FormGroup controlID='firstNameSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>First name</Col>
            <Col sm=9>
                <FormControl
                        type='text'
                        placeholder='first name'
                        onChange={@onFirstNameChange}
                        defaultValue{@props.user.firstName} />
            </Col>
        </FormGroup>

    # creating last name field
    renderLastName: ->
        <FormGroup controlID='lastNameSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Last name</Col>
            <Col sm=9>
                <FormControl
                        type='text'
                        placeholder='last name'
                        onChange={@onLastNameChange}
                        defaultValue={@props.user.lastName} />
            </Col>
        </FormGroup>

    # creating user form
    renderForm: ->
        <Form horizontal>
            {@renderUsername()}
            {@renderEmail()}
            {@renderFirstName()}
            {@renderLastName()}
        </Form>

    ###
    # Rendering modal window
    ###
    render: ->
        return null unless @props.user

        <Modal show={@props.show} onHide={@close}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Editing user {@props.user.username}
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {@renderForm()}
            </Modal.Body>

            <Modal.Footer>
                <Button onClick={@save}>Save user</Button>
                <Button onClick={@close}>Close</Button>
            </Modal.Footer>
        </Modal>

module.exports = UserEditModal

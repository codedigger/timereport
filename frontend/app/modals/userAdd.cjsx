React = require 'react'
ReactBootstrap = require 'react-bootstrap'

UserLibrary = require './../models/userLibrary.coffee'
User = require './../models/user.coffee'

{Modal, Button} = ReactBootstrap
{Form, FormGroup, Col, FormControl} = ReactBootstrap


class UserAddModal extends React.Component

    constructor: (props) ->
        super props
        @state =
            show: false

        #  user data
        @username = ''
        @firstName = ''
        @lastName = ''
        @password = ''
        @email = ''

    # Close modal window
    close: =>
        @props.close()

    # add new user
    add: =>
        user = new User
        user.username = @username
        user.firstName = @firstName
        user.lastName = @lastName
        user.email = @email
        user.save(@password)
        .then (response) =>
            @props.added user

    # field change binginds
    onUsernameChange:   (event) => @username = event.target.value
    onEmailChange:      (event) => @email = event.target.value
    onFirstNameChange:  (event) => @firstName = event.target.value
    onLastNameChange:   (event) => @lastName = event.target.value
    onPasswordChange:   (event) => @password = event.target.value

    ###
    # Rendering modal window
    ###
    # creating username field
    renderUsername: ->
        <FormGroup controlID='usernameSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Username</Col>
            <Col sm=9>
                <FormControl type='text' placeholder='username' onChange={@onUsernameChange} />
            </Col>
        </FormGroup>

    # creating email field
    renderEmail: ->
        <FormGroup controlID='emialSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Email</Col>
            <Col sm=9>
                <FormControl type='text' placeholder='email' onChange={@onEmailChange} />
            </Col>
        </FormGroup>

    # creating first name field
    renderFirstName: ->
        <FormGroup controlID='firstNameSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>First name</Col>
            <Col sm=9>
                <FormControl type='text' placeholder='first name' onChange={@onFirstNameChange} />
            </Col>
        </FormGroup>

    # creating last name field
    renderLastName: ->
        <FormGroup controlID='lastNameSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Last name</Col>
            <Col sm=9>
                <FormControl type='text' placeholder='last name' onChange={@onLastNameChange} />
            </Col>
        </FormGroup>

    # creating password field
    renderPassword: ->
        <FormGroup controlID='passwordSection'>
            <Col sm=3 componentClass={ReactBootstrap.ControlLabel}>Password</Col>
            <Col sm=9>
                <FormControl type='password' placeholder='initial user password' onChange={@onPasswordChange} />
            </Col>
        </FormGroup>

    # creating user form
    renderForm: ->
        <Form horizontal>
            { @renderUsername() }
            { @renderEmail() }
            { @renderFirstName() }
            { @renderLastName() }
            { @renderPassword() }
        </Form>

    render: ->
        <Modal show={@props.show} onHide={@close}>
            <Modal.Header closeButton>
                <Modal.Title>Modal header</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {@renderForm()}
            </Modal.Body>

            <Modal.Footer>
                <Button onClick={@add}>Add user</Button>
                <Button onClick={@close}>Close</Button>
            </Modal.Footer>
        </Modal>

module.exports = UserAddModal


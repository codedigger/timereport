React = require 'react'
ReactRouter = require 'react-router'
ReactBootstrap = require 'react-bootstrap'

UserLibrary = require './../models/userLibrary.coffee'
UserAddModal = require './../modals/userAdd.cjsx'
UserEditModal = require './../modals/userEdit.cjsx'

{withRouter} = ReactRouter
{tbody, thead, tr, td, th, div, p, span} = React.DOM
{Table, Panel, Row, Col, Button} = ReactBootstrap


class UserManagementPage extends React.Component
    @url: '/user_management'

    constructor: (props) ->
        super props
        @state =
            showAddUser: false
            showEditUser: false
            users: null

        @library = new UserLibrary
        @library.refreshUsers().then =>
            @updateUsers()

    updateUsers: ->
        @setState
            users: @library

    # show user add dialog window
    openAddUserDialog: =>
        @setState
            showAddUser: true

    # hide user add dialog window
    closeAddUserDialog: =>
        @setState
            showAddUser: false

    # show user edit dialog window
    openEditUserDialog: (user) =>
        @setState
            showEditUser: true
            editedUser: user

    # hide user edit dialog window
    closeEditUserDialog: =>
        @setState
            showEditUser: false

    # user was added to system
    userAdded: (user) =>
        @closeAddUserDialog()
        @library.push user
        @updateUsers()

    # deleting user
    userDelete: (user) ->
        user.delete().then =>
            @updateUsers() if @library.deleteUser(user)

    # user was edited
    userEdited: =>
        @closeEditUserDialog()
        @updateUsers()

    ###
    # Render part
    ###
    panelHeader: ->
        <Row>
            <Col md=11>Managing users</Col>
            <Col md=1>
                <Button onClick={@openAddUserDialog}>Add user</Button>
            </Col>
        </Row>

    userCol: (user) ->
        <tr key={user.id}>
            <td>{user.id}</td>
            <td>{user.username}</td>
            <td>{user.firstName}</td>
            <td>{user.lastName}</td>
            <td>{user.email}</td>
            <td>
                <span className='glyphicon glyphicon-pencil clickable' onClick={ => @openEditUserDialog user } />
                <span className='glyphicon glyphicon-trash clickable' onClick={ => @userDelete user } />
            </td>
        </tr>

    render: ->
        <Row>
            <Col sm=12 md=8 mdOffset=2>
                <Panel header={@panelHeader()}>
                    {if @state?.users
                        <Table id='userManageTable' condensed hover>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Email</th>
                                    <th />
                                </tr>
                            </thead>
                            <tbody>
                                {@userCol(user) for user in @state.users}
                            </tbody>
                        </Table>
                    else
                        <p>No users</p>
                    }
                </Panel>
            </Col>
            <UserAddModal show={@state.showAddUser} close={@closeAddUserDialog} added={@userAdded} />
            <UserEditModal show={@state.showEditUser} close={@closeEditUserDialog} saved={@userEdited} user={@state.editedUser} />
        </Row>

module.exports = withRouter UserManagementPage


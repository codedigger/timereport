React = require 'react'
ReactBootstrap = require 'react-bootstrap'

LoginComponent = require './../views/login.cjsx'

{Col, Row} = ReactBootstrap


class LoginPage extends React.Component
    @url: 'login'

    constructor: (props) ->
        super props

    render: ->
        <Row className='loginViewRow'>
            <Col sm=6 />
            <Col sm=4>
                <LoginComponent />
            </Col>
            <Col sm=2 />
        </Row>

module.exports = LoginPage

const webpack = require('webpack');

module.exports = {
    entry: "./app/app.cjsx",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    devtool: '#source-map',
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.cjsx$/, loaders: ['coffee', 'cjsx']},
            { test: /\.coffee$/, loader: "coffee-loader" }
        ]
    },
    devServer: {
        proxy: {
            '/api/v1*': {
                target: 'http://localhost:5000',
                secure: false
            }
        },
        historyApiFallback: true
    }
};
